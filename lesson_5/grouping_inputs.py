import tkinter as tk


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        group_1 = tk.LabelFrame(self, padx=15, pady=10,
                                text="Персональная информация")
        group_1.pack(padx=10, pady=5)

        tk.Label(group_1, text="Имя").grid(row=0)
        tk.Label(group_1, text="Фамилия").grid(row=1)
        tk.Entry(group_1).grid(row=0, column=1, sticky=tk.W)
        tk.Entry(group_1).grid(row=1, column=1, sticky=tk.W)

        group_2 = tk.LabelFrame(self, padx=15, pady=10,
                                text="Адрес")
        group_2.pack(padx=10, pady=5)

        tk.Label(group_2, text="Улица").grid(row=0)
        tk.Label(group_2, text="Город").grid(row=1)
        tk.Label(group_2, text="Индекс").grid(row=2)
        tk.Entry(group_2).grid(row=0, column=1, sticky=tk.W)
        tk.Entry(group_2).grid(row=1, column=1, sticky=tk.W)
        tk.Entry(group_2, width=8).grid(row=2, column=1,
                                        sticky=tk.W)

        self.btn_submit = tk.Button(self, text="Отправить")
        self.btn_submit.pack(padx=10, pady=10, side=tk.RIGHT)


if __name__ == "__main__":
    app = App()
    app.mainloop()
