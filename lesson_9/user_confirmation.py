import tkinter as tk
import tkinter.messagebox as mb


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.create_button(mb.askyesno, "Спросить Да/Нет",
                           "Вернет True или False")
        self.create_button(mb.askquestion, "Задать вопрос ",
                           "Вернет 'yes' или 'no'")
        self.create_button(mb.askokcancel, "Спросить Ок/Отмена",
                           "Вернет True или False")
        self.create_button(mb.askretrycancel, "Спросить Повтор/Отмена",
                           "Вернет True или False")
        self.create_button(mb.askyesnocancel, "Спросить Да/Нет/Отмена",
                           "Вернет True, False или None")

    def create_button(self, dialog, title, message):
        command = lambda: print(dialog(title, message))
        btn = tk.Button(self, text=title, command=command)
        btn.pack(padx=40, pady=5, expand=True, fill=tk.BOTH)


if __name__ == "__main__":
    app = App()
    app.mainloop()